import math
import resource
import sys
from functools import partial
from threading import Thread

from PyQt5.QtWidgets import QApplication


from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtCore import QRect, Qt, QTimer
from PyQt5.QtGui import QPen, QBrush, QColor

from client import BattleCityConnection




class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        self.central_widget = QtWidgets.QWidget(self)
        self.setCentralWidget(self.central_widget)

        self.setWindowTitle("NeuralNetwork viewer")
        self.resize(300, 300)
        self.pressed_keys = set()
        self.board = ''

        self.timer = QTimer(self)
        self.timer.timeout.connect(self.repaint)
        self.timer.start(200)

    def paintEvent(self, event):
        if not self.board:
            return
        n = int(math.sqrt(len(self.board)))
        board = '\n'.join(self.board[i:i + n] for i in range(0, len(self.board), n))
        print(board)


        qp = QtGui.QPainter()
        qp.begin(self)
        # qp.drawText(QRect(0, 0, 100, 100), Qt.AlignLeft, "Qt\nhello")
        # if self.board:
        QtGui.QFont.TypeWriter
        qp.drawText(QRect(0, 0, self.width(), self.height()), Qt.AlignLeft, board)
        qp.end()

    def keyPressEvent(self, event: QtGui.QKeyEvent):
        # print('press', event.key())
        self.pressed_keys.add(event.key())

        print(Qt.Key_Up)
        print(Qt.Key_Up in self.pressed_keys)

    def keyReleaseEvent(self, event: QtGui.QKeyEvent):
        # print('release', event.key())
        if event.key() in self.pressed_keys:
            self.pressed_keys.remove(event.key())

    def turn(self, board):
        self.board = board
        action = ['act'] if Qt.Key_Space in self.pressed_keys else []

        if Qt.Key_Up in self.pressed_keys:
            action.append('up')
        elif Qt.Key_Down in self.pressed_keys:
            action.append('down')
        elif Qt.Key_Left in self.pressed_keys:
            action.append('left')
        elif Qt.Key_Right in self.pressed_keys:
            action.append('right')

        print('action: ', action)
        return ','.join(action)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    mySW = MainWindow()

    url = "http://codebattle-pro-2020s1.westeurope.cloudapp.azure.com/codenjoy-contest/board/player/sdjktacrm4iqkl3ae8d8?code=5025901194386575442&gameName=snakebattle"
    ws = BattleCityConnection(url, mySW)
    ws.connect()

    t = Thread(target=ws.run_forever)
    t.start()

    mySW.show()
    sys.exit(app.exec_())
