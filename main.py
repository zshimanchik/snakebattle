import logging

from client import BattleCityConnection
from player import Player
from colorama import init

init(autoreset=True)

logging.basicConfig(format='%(asctime)s %(levelname)s:%(message)s', level=logging.INFO)

RESTART_ON_EXCEPTION = False


def main():
    try:
        # Testbot
        url = 'http://codebattle-pro-2020s1.westeurope.cloudapp.azure.com/codenjoy-contest/board/player/z4lxi9dcxrzw7n2wwjk1?code=8853386976345746662'
        player = Player(process_count=4)
        ws = BattleCityConnection(url, player)
        ws.connect()
        ws.run_forever()
    except Exception as ex:
        print(ex)
    finally:
        try:
            print('terminating player')
            player.terminate()
        except Exception as ex:
            print(ex)
        try:
            ws.close()
        except Exception as ex:
            print(ex)


if __name__ == '__main__':
    main()
    while RESTART_ON_EXCEPTION:
        print("====restart====")
        main()
