import unittest

from engine.world import BoardInfo
from player import Player
from tests.uutils import strip_board_str


class TestSimpleSequence(unittest.TestCase):

    def test_1(self):
        board_str = '''
            ☼☼☼☼☼☼☼
            ☼     ☼
            ☼ ○☼╔╕☼
            ☼  ♥╝ ☼
            ☼☼   ☼☼
            ☼     ☼
            ☼☼☼☼☼☼☼ 
        '''
        board_str = ''.join(line.strip() for line in board_str.splitlines())

        player = Player(process_count=0)
        result = player.turn(board_str, calc_amount=40)

        self.assertEqual(result, 'left')

    def test_no_space(self):
        board_str = '''
            ☼☼☼☼☼☼☼
            ☼ ☼   ☼
            ☼ ○☼╔╕☼
            ☼  ♥╝ ☼
            ☼☼   ☼☼
            ☼     ☼
            ☼☼☼☼☼☼☼
        '''
        board_str = ''.join(line.strip() for line in board_str.splitlines())

        player = Player(process_count=0)
        result = player.turn(board_str, calc_amount=40)

        self.assertNotEqual(result, 'left')
        print(result)

    def test_dead_end(self):
        board_str = '''
            ☼☼☼☼☼☼☼
            ☼ ☼   ☼
            ☼ ☼☼╔╕☼
            ☼☼○♥╝ ☼
            ☼☼☼  ☼☼
            ☼     ☼
            ☼☼☼☼☼☼☼
        '''
        board_str = ''.join(line.strip() for line in board_str.splitlines())

        player = Player(process_count=0)
        result = player.turn(board_str, calc_amount=40)

        self.assertNotEqual(result, 'left')
        print(result)

    def test_take_closest_apple_on_the_way_to_rage(self):
        board_str = '''
            ☼☼☼☼☼☼☼
            ☼ ☼   ☼
            ☼ ☼☼╔╕☼
            ☼○ ♥╝ ☼
            ☼    ☼☼
            ☼®    ☼
            ☼☼☼☼☼☼☼
        '''
        board_str = ''.join(line.strip() for line in board_str.splitlines())

        player = Player(process_count=0)
        result = player.turn(board_str, calc_amount=40)

        self.assertEqual(result, 'left')
        print(result)


class TestBiteEnemy(unittest.TestCase):

    # def test_1(self):
    #     board_str = '''
    #         ☼☼☼☼☼☼☼☼
    #         ☼      ☼
    #         ☼ ☼    ☼
    #         ☼  ☼   ☼
    #         ☼ ┌┐   ☼
    #         ☼×┘♣♥╗ ☼
    #         ☼☼   ╚╕☼
    #         ☼☼☼☼☼☼☼☼
    #     '''
    #     board_str = ''.join(line.strip() for line in board_str.splitlines())
    #
    #     player = Player(process_count=0)
    #     result = player.turn(board_str, calc_amount=40)
    #
    #     self.assertEqual(result, 'left')

    def test_bite_closer_to_the_head(self):
        board_str = '''
            ☼☼☼☼☼☼☼☼
            ☼      ☼
            ☼ ☼    ☼
            ☼  ☼♣  ☼
            ☼  ┌┘  ☼
            ☼×─┘♥╗ ☼
            ☼☼   ╚╕☼
            ☼☼☼☼☼☼☼☼
        '''
        board_str = ''.join(line.strip() for line in board_str.splitlines())

        player = Player(process_count=0)
        player.board_info = BoardInfo(0, 2)
        result = player.turn(board_str, calc_amount=40)

        self.assertEqual(result, 'up')

    def test_both_not_evil_before_tonnel(self):
        board_str = '''
            ☼☼☼☼☼☼☼☼
            ☼      ☼
            ☼ ☼    ☼
            ☼ ¤☼   ☼
            ☼ │☼  ╓☼
            ☼ ˅ ◄╗║☼
            ☼  ☼ ╚╝☼
            ☼☼☼☼☼☼☼☼
        '''
        board_str = ''.join(line.strip() for line in board_str.splitlines())

        player = Player(process_count=0)
        result = player.turn(board_str, calc_amount=40)

        self.assertNotEqual(result, 'left')

    def test_take_closest_apple_1(self):
        board_str = '''
            ☼☼☼☼☼☼☼☼☼☼☼☼☼☼
            ☼          ╔╕☼
            ☼         ♥╝ ☼
            ☼            ☼
            ☼           ☼☼
            ☼           ☼☼
            ☼           ☼☼
            ☼           ☼☼
            ☼           ☼☼
            ☼           ☼☼
            ☼           ☼☼
            ☼           ☼☼
            ☼       ○○   ☼
            ☼☼☼☼☼☼☼☼☼☼☼☼☼☼
        '''
        board_str = ''.join(line.strip() for line in board_str.splitlines())

        player = Player(process_count=0)
        result = player.turn(board_str, calc_amount=40)

        self.assertEqual(result, 'down')
        print(result)



    def test_snake_fight_body_me_evil_on_map_but_not_in_board_info(self):
        board_str = strip_board_str('''
            ☼☼☼☼☼☼☼☼
            ☼      ☼
            ☼ ☼    ☼
            ☼ ¤☼   ☼
            ☼ │   ╓☼
            ☼ │♥═╗║☼
            ☼<┘☼ ╚╝☼
            ☼☼☼☼☼☼☼☼
        ''')
        player = Player(process_count=0)
        player.board_info = BoardInfo(0, 1)
        result = player.turn(board_str, calc_amount=40)

        self.assertNotEqual(result, 'left')



    def test_snake_fight_1(self):
        board_str = strip_board_str('''
            ☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼
            ☼☼                    ●        ☼
            ☼☼   ○  ®                      ☼
            ☼☼  ☼☼☼  ☼☼  ☼☼☼☼  ☼  ☼☼☼☼  ☼  ☼
            ☼#  ☼    ®☼  ☼○○☼  ☼  ☼     ☼  ☼
            ☼☼  ☼  ☼● ☼  ☼    ☼☼  ☼  ☼  ☼  ☼
            ☼☼  ☼   ☼    ☼        ☼    ☼☼  ☼
            ☼☼  ☼☼       ☼☼                ☼
            ☼☼        ☼      ☼ ☼      ☼ ●  ☼
            ☼#    ☼   ☼  ☼     ☼  ☼○   ○☼  ☼
            ☼☼● ☼☼☼○ ☼☼  ☼☼☼  ☼☼  ☼☼☼® ☼☼  ☼
            ☼☼ ®®                          ☼
            ☼☼®           ○             ®  ☼
            ☼☼ ○☼☼☼☼  ☼○ ☼☼☼☼╓ ☼  ☼☼☼  ☼☼  ☼
            ☼#  ☼  ☼  ☼  ☼╔══╝ ☼  ☼     ☼  ☼
            ☼☼  ☼®®  ☼☼  ☼║ ☼  ☼  ☼  ☼● ☼ ○☼
            ☼☼  ☼  ●     ☼║   ☼☼  ☼   ☼    ☼
            ☼☼  ☼☼      ╔═╝       ☼☼       ☼
            ☼☼      ☼ ☼ ║    ☼          ☼  ☼
            ☼#  ☼    ○☼ ║☼     ☼    ☼ ○ ☼  ☼
            ☼☼○ ☼☼☼  ☼☼ ║☼☼☼  ☼☼  ☼☼☼  ☼☼® ☼
            ☼☼          ║                  ☼
            ☼☼          ╚♥                 ☼
            ☼☼  ☼☼☼☼  ☼☼☼☼♣ ☼☼☼☼  ☼☼☼☼  ☼  ☼
            ☼☼  ☼         └┐☼┌─┐    ☼   ☼  ☼
            ☼#     ☼       └─┘☼│  ☼   ☼    ☼
            ☼☼          ☼    ☼┌┘      ☼●   ☼
            ☼☼  ☼    ☼    ○   │ ☼     ○ ☼  ☼
            ☼☼  ☼  ☼☼☼☼  ☼☼☼☼☼│ ☼☼☼☼  ☼☼☼  ☼
            ☼☼                └────ö       ☼
            ☼☼          ○                  ☼
            ☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼
        ''')
        player = Player(process_count=0)
        player.board_info = BoardInfo(tick=154, evil_countdown=6)
        result = player.turn(board_str, calc_amount=40)
        print(result)

        self.assertNotEqual(result, 'right')
