
def strip_board_str(board_str):
    return ''.join(line.strip() for line in board_str.splitlines())
