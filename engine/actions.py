from enum import Enum


class Action(Enum):
    LEFT = 'left'
    RIGHT = 'right'
    UP = 'up'
    DOWN = 'down'
    STOP = 'stop'
    ACT_LEFT = 'act,left'
    ACT_RIGHT = 'act,right'
    ACT_UP = 'act,up'
    ACT_DOWN = 'act,down'

    def __str__(self):
        return f'<{self.value.upper()}>'

    def __repr__(self):
        return str(self)


