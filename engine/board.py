from math import sqrt

from cached_property import cached_property

from engine import element
from engine.point import Point, Direction


class Board:
    """ Class describes the Board field for Bomberman game."""

    def __init__(self, board_string):
        self._string = board_string.replace('\n', '')
        self._len = len(self._string)  # the length of the string
        self._size = int(sqrt(self._len))  # size of the board

    def get_point_by_shift(self, shift):
        return Point(shift % self._size, int(shift / self._size))

    def find_first_element(self, *element_types):
        _result = []
        for i in range(self._size * self._size):
            point = self.get_point_by_shift(i)
            for type in element_types:
                if self.has_element_at(point, type):
                    return point
        return None

    @cached_property
    def get_my_head(self):
        return self.get_my_snake[0]

    @cached_property
    def get_my_head_el(self):
        return self.get_element_at(self.get_my_head)

    def _find_all(self, *element_types):
        """ Returns the list of points for the given element type."""
        _points = []
        for i in range(self._size * self._size):
            point = self.get_point_by_shift(i)
            for type in element_types:
                if self.has_element_at(point, type):
                    _points.append(point)
        return _points

    @cached_property
    def get_apples(self):
        return self._find_all(element.APPLE)

    @cached_property
    def get_enemy_heads(self):
        return self._find_all(element.ENEMY_HEADS)

    @cached_property
    def get_walls(self):
        return self._find_all(element.WALL)

    @cached_property
    def get_stones(self):
        return self._find_all(element.STONE)

    @cached_property
    def get_barriers(self):
        """ Return the list of barriers Points."""
        points = set()
        points.update(self._find_all(element.WALL, element.START_FLOOR, element.ENEMY_HEAD_SLEEP,
                                     element.ENEMY_TAIL_INACTIVE, element.TAIL_INACTIVE))
        return list(points)

    def is_barrier_at(self, point):
        return point in self.get_barriers

    def get_apples(self):
        return self._find_all(element.APPLE)

    # @cached_property
    # def am_i_evil(self):
    #     return self.get_my_head_el == element.HEAD_EVIL

    @cached_property
    def am_i_flying(self):
        return self.get_my_head_el == element.HEAD_FLY

    @cached_property
    def is_game_active(self):
        # return self.find_first_element(element.HEAD_DEAD, element.HEAD_SLEEP) is None
        return self.find_first_element(*element.HEADS_ACTIVE) is not None

    def get_flying_pills(self):
        return self._find_all(element.FLYING_PILL)

    def get_furry_pills(self):
        return self._find_all(element.FURY_PILLS)

    def get_gold(self):
        return self._find_all(element.GOLD)

    def get_start_points(self):
        return self._find_all(element.START_FLOOR)

    def get_element_at(self, point):
        """ Return an Element object at coordinates x,y."""
        return self._string[self._xy2strpos(point.x, point.y)]

    def has_element_at(self, point, element_object):
        if point.is_out_of_board(self._size):
            return False
        return element_object == self.get_element_at(point)

    def find_element(self, type):
        for i in range(self._size * self._size):
            point = self.get_point_by_shift(i)
            if self.has_element_at(point, type):
                return point
        return None

    def get_shift_by_point(self, point):
        return point.y * self._size + point.x

    def _strpos2pt(self, strpos):
        return Point(*self._strpos2xy(strpos))

    def _strpos2xy(self, strpos):
        return (strpos % self._size, strpos // self._size)

    def _xy2strpos(self, x, y):
        return self._size * y + x

    def print_board(self):
        print(self._line_by_line())

    def _line_by_line(self):
        return '\n'.join([self._string[i:i + self._size]
                          for i in range(0, self._len, self._size)])

    def to_string(self):
        return ("Board:\n{brd}".format(brd=self._line_by_line()))

    @cached_property
    def get_my_snake(self) -> list:
        result = []
        tail_pos = self.find_first_element(*element.TAILS_ACTIVE)
        tail_el = self.get_element_at(tail_pos)
        result.append(tail_pos)
        cur_dir = {
            element.TAIL_END_DOWN: Direction.UP,
            element.TAIL_END_LEFT: Direction.RIGHT,
            element.TAIL_END_UP: Direction.DOWN,
            element.TAIL_END_RIGHT: Direction.LEFT,
        }[tail_el]
        cur_pos = tail_pos + cur_dir.to_point()
        cur_el = self.get_element_at(cur_pos)
        result.append(cur_pos)
        while cur_el not in element.HEADS:
            cur_dir = { #  body, prev_direction -> next_direction
                (element.BODY_HORIZONTAL, Direction.LEFT): Direction.LEFT,
                (element.BODY_HORIZONTAL, Direction.RIGHT): Direction.RIGHT,

                (element.BODY_VERTICAL, Direction.UP): Direction.UP,
                (element.BODY_VERTICAL, Direction.DOWN): Direction.DOWN,

                (element.BODY_LEFT_DOWN, Direction.RIGHT): Direction.DOWN,
                (element.BODY_LEFT_DOWN, Direction.UP): Direction.LEFT,

                (element.BODY_LEFT_UP, Direction.RIGHT): Direction.UP,
                (element.BODY_LEFT_UP, Direction.DOWN): Direction.LEFT,

                (element.BODY_RIGHT_DOWN, Direction.LEFT): Direction.DOWN,
                (element.BODY_RIGHT_DOWN, Direction.UP): Direction.RIGHT,

                (element.BODY_RIGHT_UP, Direction.LEFT): Direction.UP,
                (element.BODY_RIGHT_UP, Direction.DOWN): Direction.RIGHT,
            }[cur_el, cur_dir]
            cur_pos = cur_pos + cur_dir.to_point()
            cur_el = self.get_element_at(cur_pos)
            result.append(cur_pos)
        result.reverse()
        return result

    def draw_snake(self, snake, new_board, evil):
        # draw head
        if evil:
            i = self.get_shift_by_point(snake[0])
            new_board[i] = element.HEAD_EVIL
        else:
            dir = (snake[0] - snake[1]).to_direction()
            head_el = {
                Direction.UP: element.HEAD_UP,
                Direction.RIGHT: element.HEAD_RIGHT,
                Direction.DOWN: element.HEAD_DOWN,
                Direction.LEFT: element.HEAD_LEFT
            }[dir]
            i = self.get_shift_by_point(snake[0])
            new_board[i] = head_el

        # draw body
        for body_i in range(1, len(snake)-1):
            cur_head_dir = (snake[body_i-1] - snake[body_i]).to_direction()
            cur_tail_dir = (snake[body_i] - snake[body_i+1]).to_direction()
            body_el = {
                (Direction.LEFT, Direction.LEFT): element.BODY_HORIZONTAL,
                (Direction.RIGHT, Direction.RIGHT): element.BODY_HORIZONTAL,

                (Direction.UP, Direction.UP): element.BODY_VERTICAL,
                (Direction.DOWN, Direction.DOWN): element.BODY_VERTICAL,

                (Direction.LEFT, Direction.UP): element.BODY_LEFT_DOWN,
                (Direction.DOWN, Direction.RIGHT): element.BODY_LEFT_DOWN,

                (Direction.UP, Direction.RIGHT): element.BODY_LEFT_UP,
                (Direction.LEFT, Direction.DOWN): element.BODY_LEFT_UP,

                (Direction.RIGHT, Direction.UP): element.BODY_RIGHT_DOWN,
                (Direction.DOWN, Direction.LEFT): element.BODY_RIGHT_DOWN,

                (Direction.UP, Direction.LEFT): element.BODY_RIGHT_UP,
                (Direction.RIGHT, Direction.DOWN): element.BODY_RIGHT_UP,
            }[cur_head_dir, cur_tail_dir]
            i = self.get_shift_by_point(snake[body_i])
            new_board[i] = body_el

        #draw tail
        dir = (snake[-2] - snake[-1]).to_direction()
        tail_el = {
            Direction.UP: element.TAIL_END_DOWN,
            Direction.RIGHT: element.TAIL_END_LEFT,
            Direction.DOWN: element.TAIL_END_UP,
            Direction.LEFT: element.TAIL_END_RIGHT,
        }[dir]
        i = self.get_shift_by_point(snake[-1])
        new_board[i] = tail_el

        return new_board

    def draw_enemy_snake(self, snake, new_board, evil):
        # draw head
        if evil:
            i = self.get_shift_by_point(snake[0])
            new_board[i] = element.ENEMY_HEAD_EVIL
        else:
            dir = (snake[0] - snake[1]).to_direction()
            head_el = {
                Direction.UP: element.ENEMY_HEAD_UP,
                Direction.RIGHT: element.ENEMY_HEAD_RIGHT,
                Direction.DOWN: element.ENEMY_HEAD_DOWN,
                Direction.LEFT: element.ENEMY_HEAD_LEFT
            }[dir]
            i = self.get_shift_by_point(snake[0])
            new_board[i] = head_el

        # draw body
        for body_i in range(1, len(snake)-1):
            cur_head_dir = (snake[body_i-1] - snake[body_i]).to_direction()
            cur_tail_dir = (snake[body_i] - snake[body_i+1]).to_direction()
            body_el = {
                (Direction.LEFT, Direction.LEFT): element.ENEMY_BODY_HORIZONTAL,
                (Direction.RIGHT, Direction.RIGHT): element.ENEMY_BODY_HORIZONTAL,

                (Direction.UP, Direction.UP): element.ENEMY_BODY_VERTICAL,
                (Direction.DOWN, Direction.DOWN): element.ENEMY_BODY_VERTICAL,

                (Direction.LEFT, Direction.UP): element.ENEMY_BODY_LEFT_DOWN,
                (Direction.DOWN, Direction.RIGHT): element.ENEMY_BODY_LEFT_DOWN,

                (Direction.UP, Direction.RIGHT): element.ENEMY_BODY_LEFT_UP,
                (Direction.LEFT, Direction.DOWN): element.ENEMY_BODY_LEFT_UP,

                (Direction.RIGHT, Direction.UP): element.ENEMY_BODY_RIGHT_DOWN,
                (Direction.DOWN, Direction.LEFT): element.ENEMY_BODY_RIGHT_DOWN,

                (Direction.UP, Direction.LEFT): element.ENEMY_BODY_RIGHT_UP,
                (Direction.RIGHT, Direction.DOWN): element.ENEMY_BODY_RIGHT_UP,
            }[cur_head_dir, cur_tail_dir]
            i = self.get_shift_by_point(snake[body_i])
            new_board[i] = body_el

        #draw tail
        dir = (snake[-2] - snake[-1]).to_direction()
        tail_el = {
            Direction.UP: element.ENEMY_TAIL_END_DOWN,
            Direction.RIGHT: element.ENEMY_TAIL_END_LEFT,
            Direction.DOWN: element.ENEMY_TAIL_END_UP,
            Direction.LEFT: element.ENEMY_TAIL_END_RIGHT,
        }[dir]
        i = self.get_shift_by_point(snake[-1])
        new_board[i] = tail_el

        return new_board

    def get_enemy_snake(self, pos):
        el = self.get_element_at(pos)
        if el in element.ENEMY_TAILS:
            return self._build_enemy_snake_from_tail(pos, el)
        elif el in element.ENEMY_HEADS:
            return self._build_enemy_snake_from_head(pos, el)
        elif el in element.ENEMY_BODIES:
            return self._build_enemy_snake_from_body(pos, el)

    def _build_enemy_snake_from_tail(self, pos, el):
        dir = {
            element.ENEMY_TAIL_END_DOWN: Direction.UP,
            element.ENEMY_TAIL_END_LEFT: Direction.RIGHT,
            element.ENEMY_TAIL_END_UP: Direction.DOWN,
            element.ENEMY_TAIL_END_RIGHT: Direction.LEFT,
        }[el]
        snake_start = [pos]
        snake = self._build_enemy_in_direction(pos + dir, dir, element.ENEMY_HEADS)
        snake = snake_start + snake
        snake.reverse()
        return snake

    def _build_enemy_snake_from_head(self, pos, el):
        dir = {
            element.ENEMY_HEAD_DOWN: Direction.UP,
            element.ENEMY_HEAD_LEFT: Direction.RIGHT,
            element.ENEMY_HEAD_RIGHT: Direction.LEFT,
            element.ENEMY_HEAD_UP: Direction.DOWN,
        }.get(el)

        if dir is None:
            # trying to find body around
            for d in Direction:
                next_pos = pos + d.to_point()
                next_el = self.get_element_at(next_pos)
                matches = {
                    (Direction.UP, element.ENEMY_BODY_VERTICAL),
                    (Direction.UP, element.ENEMY_BODY_LEFT_DOWN),
                    (Direction.UP, element.ENEMY_BODY_RIGHT_DOWN),
                    (Direction.UP, element.ENEMY_TAIL_END_UP),

                    (Direction.DOWN, element.ENEMY_BODY_VERTICAL),
                    (Direction.DOWN, element.ENEMY_BODY_LEFT_UP),
                    (Direction.DOWN, element.ENEMY_BODY_RIGHT_UP),
                    (Direction.DOWN, element.ENEMY_TAIL_END_DOWN),

                    (Direction.LEFT, element.ENEMY_BODY_HORIZONTAL),
                    (Direction.LEFT, element.ENEMY_BODY_RIGHT_UP),
                    (Direction.LEFT, element.ENEMY_BODY_RIGHT_DOWN),
                    (Direction.LEFT, element.ENEMY_TAIL_END_LEFT),

                    (Direction.RIGHT, element.ENEMY_BODY_HORIZONTAL),
                    (Direction.RIGHT, element.ENEMY_BODY_LEFT_UP),
                    (Direction.RIGHT, element.ENEMY_BODY_LEFT_DOWN),
                    (Direction.RIGHT, element.ENEMY_TAIL_END_RIGHT),
                }
                if (d, next_el) in matches:
                    dir = d
                    break
        if dir is None:
            raise Exception("Can't find enemy snake direction from head")
        start_snake = [pos]
        snake = self._build_enemy_in_direction(pos + dir, dir, element.ENEMY_TAILS)
        return start_snake + snake

    def _build_enemy_snake_from_body(self, pos, el):
        dirs = {
            element.ENEMY_BODY_HORIZONTAL: (Direction.LEFT, Direction.RIGHT),
            element.ENEMY_BODY_VERTICAL: (Direction.UP, Direction.DOWN),
            element.ENEMY_BODY_LEFT_DOWN: (Direction.LEFT, Direction.DOWN),
            element.ENEMY_BODY_LEFT_UP: (Direction.LEFT, Direction.UP),
            element.ENEMY_BODY_RIGHT_DOWN: (Direction.RIGHT, Direction.DOWN),
            element.ENEMY_BODY_RIGHT_UP: (Direction.RIGHT, Direction.UP),
        }[el]
        snake_part_2 = [pos]
        snake_part_1 = self._build_enemy_in_direction(pos + dirs[0], dirs[0], element.ENEMY_HEADS_AND_TAILS)
        snake_part_3 = self._build_enemy_in_direction(pos + dirs[1], dirs[1], element.ENEMY_HEADS_AND_TAILS)

        if self.get_element_at(snake_part_1[-1]) in element.ENEMY_HEADS:
            snake_part_1.reverse()
            snake = snake_part_1 + snake_part_2 + snake_part_3
        elif self.get_element_at(snake_part_3[-1]) in element.ENEMY_HEADS:
            snake_part_3.reverse()
            snake = snake_part_3 + snake_part_2 + snake_part_1
        else:
            raise Exception("Can't build enemy snake")
        return snake


    def _build_enemy_in_direction(self, start_pos, dir, terminate_elements) -> list:
        result = []
        cur_pos = start_pos
        cur_el = self.get_element_at(cur_pos)
        cur_dir = dir
        result.append(cur_pos)
        while cur_el not in terminate_elements:
            cur_dir = {  # body, prev_direction -> next_direction
                (element.ENEMY_BODY_HORIZONTAL, Direction.LEFT): Direction.LEFT,
                (element.ENEMY_BODY_HORIZONTAL, Direction.RIGHT): Direction.RIGHT,

                (element.ENEMY_BODY_VERTICAL, Direction.UP): Direction.UP,
                (element.ENEMY_BODY_VERTICAL, Direction.DOWN): Direction.DOWN,

                (element.ENEMY_BODY_LEFT_DOWN, Direction.RIGHT): Direction.DOWN,
                (element.ENEMY_BODY_LEFT_DOWN, Direction.UP): Direction.LEFT,

                (element.ENEMY_BODY_LEFT_UP, Direction.RIGHT): Direction.UP,
                (element.ENEMY_BODY_LEFT_UP, Direction.DOWN): Direction.LEFT,

                (element.ENEMY_BODY_RIGHT_DOWN, Direction.LEFT): Direction.DOWN,
                (element.ENEMY_BODY_RIGHT_DOWN, Direction.UP): Direction.RIGHT,

                (element.ENEMY_BODY_RIGHT_UP, Direction.LEFT): Direction.UP,
                (element.ENEMY_BODY_RIGHT_UP, Direction.DOWN): Direction.RIGHT,
            }[cur_el, cur_dir]
            cur_pos = cur_pos + cur_dir.to_point()
            cur_el = self.get_element_at(cur_pos)
            result.append(cur_pos)
        return result


if __name__ == '__main__':
    raise RuntimeError("This module is not designed to be ran from CLI")
