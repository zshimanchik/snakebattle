import math
from dataclasses import dataclass
from enum import Enum

from engine.actions import Action




class Direction(Enum):
    UP = 'up'
    RIGHT = 'right'
    DOWN = 'down'
    LEFT = 'left'

    def to_point(self):
        self_to_vec = {
            Direction.UP: Point(0, -1),
            Direction.RIGHT: Point(1, 0),
            Direction.DOWN: Point(0, 1),
            Direction.LEFT: Point(-1, 0),
        }
        return self_to_vec[self]

    @staticmethod
    def from_action(action):
        action_to_dir = {
            Action.UP: Direction.UP,
            Action.RIGHT: Direction.RIGHT,
            Action.DOWN: Direction.DOWN,
            Action.LEFT: Direction.LEFT,
        }
        return action_to_dir[action]

    def get_ascii_arrow(self):
        return {
            Direction.UP: '↑',
            Direction.RIGHT: '→',
            Direction.DOWN: '↓',
            Direction.LEFT: '←',
        }[self]


@dataclass(frozen=True)
class Point:
    x: int
    y: int

    def __add__(self, other):
        if isinstance(other, Point):
            return Point(self.x + other.x, self.y + other.y)
        elif isinstance(other, Direction):
            return self + other.to_point()
        else:
            raise ValueError()

    def __sub__(self, other):
        if isinstance(other, Point):
            return Point(self.x - other.x, self.y - other.y)
        elif isinstance(other, Direction):
            return self - other.to_point()
        else:
            raise ValueError()

    def __mul__(self, other):
        if isinstance(other, int):
            return Point(self.x * other, self.y * other)
        else:
            raise ValueError()

    def length(self):
        return math.hypot(self.x, self.y)

    def get_neighbours(self):
        for dir in Direction:
            yield self + dir.get_delta()

    def is_out_of_board(self, board_size):
        return self.x >= board_size or self.y >= board_size or self.x < 0 or self.y < 0

    def to_direction(self):
        if self.x == 0 and self.y < 0:
            return Direction.UP
        elif self.x == 0 and self.y > 0:
            return Direction.DOWN
        elif self.y == 0 and self.x > 0:
            return Direction.RIGHT
        elif self.y == 0 and self.x < 0:
            return Direction.LEFT
        else:
            raise ValueError()


# class Point:
#     """ Describes a point on board."""
#     def __init__(self, x=0, y=0):
#         self._x = int(x)
#         self._y = int(y)
#
#     def __key(self):
#         return self._x, self._y
#
#     def __str__(self):
#         return self.to_string()
#
#     def __repr__(self):
#         return self.to_string()
#
#     def __eq__(self, other_point):
#         return self.__key() == other_point.__key()
#
#     def __hash__(self):
#         return hash(self.__key())
#
#     def get_x(self):
#         return self._x
#
#     def get_y(self):
#         return self._y
#
#     def to_string(self):
#         return "[{},{}]".format(self._x, self._y)
#
#     # Returns new BoardPoint object shifted bottom "delta" points
#     def shift_bottom(self, delta):
#         return Point(self._x, self._y + delta)
#
#     # Returns new BoardPoint object shifted top "delta" points
#     def shift_top(self, delta):
#         return Point(self._x, self._y - delta)
#
#     # Returns new BoardPoint object shifted right to "delta" points
#     def shift_right(self, delta):
#         return Point(self._x + delta, self._y)
#
#     # Returns new BoardPoint object shifted left to "delta" points
#     def shift_left(self, delta):
#         return Point(self._x - delta, self._y)
#
#     def shift_by_action(self, action):
#         if action == Action.UP:
#             return self.shift_top(1)
#         elif action == Action.RIGHT:
#             return self.shift_right(1)
#         elif action == Action.DOWN:
#             return self.shift_bottom(1)
#         elif action == Action.LEFT:
#             return self.shift_left(1)
#         else:
#             raise ValueError("Unknown Action")
#
#     # Checks is current point on board or out of range.
#     # @param boardSize Board size to compare
#     def is_out_of_board(self, board_size):
#         return self._x >= board_size or self._y >= board_size or self._x < 0 or self._y < 0


# POINT_UP = Point(0, -1)
# POINT_RIGHT = Point(1, 0)
# POINT_DOWN = Point(0, 1)
# POINT_LEFT = Point(-1, 0)
# DELTA_POINTS = [POINT_UP, POINT_RIGHT, POINT_DOWN, POINT_LEFT]
