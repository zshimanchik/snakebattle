from dataclasses import dataclass

from engine import element
from engine.board import Board
from engine.point import Point, Direction

SNAKE_REWARD = 15
FURY_COUNTDOWN = 10


@dataclass(frozen=True)
class BoardInfo:
    tick: int
    evil_countdown: int


class World:
    def __init__(self, board, board_info, action, depth=1):
        """
        :param board:
        :param board_info:
        :param action:
        :param depth: depth from task - how much ticks away in future this board from actual situation
        """
        assert depth > 0

        self.board = board
        self.action = action
        self.depth = depth
        self.board_info = board_info

    def calculate(self):
        if not self.board.is_game_active:
            return 0, None, self.board_info

        # i_am_evil = head_el == element.HEAD_EVIL
        new_board_info = BoardInfo(self.board_info.tick + 1, max(0, self.board_info.evil_countdown - 1))
        i_am_evil = new_board_info.evil_countdown > 0

        head = self.board.get_my_head
        head_el = self.board.get_element_at(head)
        if head_el == element.HEAD_SLEEP:
            return 0, None, self.board_info

        my_snake = self.board.get_my_snake[:]
        # print('snake', snake)
        # next_head_pos = head.shift_by_action(self.action)
        next_head_pos = head + Direction.from_action(self.action)
        next_el = self.board.get_element_at(next_head_pos)

        if next_el in [element.WALL, element.START_FLOOR]:
            return -100, None, new_board_info
        elif next_el in element.BODIES + element.TAILS:
            return -40, None, new_board_info # todo cut tail
        elif next_el in [element.NONE, element.ENEMY_HEAD_DEAD]:
            # todo check if nearby evil enemy with longer length
            new_board = self.move_me(i_am_evil)
            return 0, new_board, new_board_info
        elif next_el in element.ENEMY:
            enemy_snake = self.board.get_enemy_snake(next_head_pos)
            enemy_is_evil = self.board.get_element_at(enemy_snake[0]) == element.ENEMY_HEAD_EVIL

            element_from_head_with_depth = 0
            for element_from_head_with_depth, point in enumerate(enemy_snake):
                if point == next_head_pos:
                    break
            element_from_head_with_depth += self.depth  # it will change on next tick, so consider depth here

            if i_am_evil and not enemy_is_evil:
                if element_from_head_with_depth <= 1:  # Neck
                    # remove enemy
                    reward = len(enemy_snake) * SNAKE_REWARD
                    new_board = list(self.board._string)
                    self.wipe_snake_from_board(new_board, enemy_snake)
                    self.wipe_snake_from_board(new_board, my_snake)
                    self.move_snake(my_snake, Direction.from_action(self.action))
                    self.board.draw_snake(my_snake, new_board, i_am_evil)
                    return reward, Board(''.join(new_board)), new_board_info
                else:  # Body
                    # cut enemy snake on board
                    reward = max(0, len(enemy_snake) - element_from_head_with_depth) * SNAKE_REWARD
                    new_board = list(self.board._string)
                    self.wipe_snake_from_board(new_board, enemy_snake)
                    self.wipe_snake_from_board(new_board, my_snake)
                    enemy_snake = enemy_snake[:element_from_head_with_depth-self.depth]
                    if len(enemy_snake) < 2:
                        return reward, None, new_board_info
                    self.move_snake(my_snake, Direction.from_action(self.action))
                    self.board.draw_enemy_snake(enemy_snake, new_board, enemy_is_evil)
                    self.board.draw_snake(my_snake, new_board, i_am_evil)
                    return reward, Board(''.join(new_board)), new_board_info
            elif i_am_evil and enemy_is_evil:
                if element_from_head_with_depth <= 1:  # Neck
                    if len(my_snake) - len(enemy_snake) > 2:  # I'll survive
                        # remove enemy and cut my snake
                        reward = len(enemy_snake) * SNAKE_REWARD
                        new_board = self.remove_enemy_and_cut_me(my_snake, i_am_evil, enemy_snake)
                        return reward, new_board, new_board_info
                    else:
                        return -140, None, new_board_info
                else:  # Body
                    reward = max(0, len(enemy_snake) - element_from_head_with_depth - self.depth) * SNAKE_REWARD
                    # cut snake on board
                    new_board = list(self.board._string)
                    self.wipe_snake_from_board(new_board, enemy_snake)
                    self.wipe_snake_from_board(new_board, my_snake)
                    enemy_snake = enemy_snake[:element_from_head_with_depth-self.depth]
                    if len(enemy_snake) < 2:
                        return reward, None, new_board_info
                    self.move_snake(my_snake, Direction.from_action(self.action))
                    self.board.draw_enemy_snake(enemy_snake, new_board, enemy_is_evil)
                    self.board.draw_snake(my_snake, new_board, i_am_evil)
                    return reward, Board(''.join(new_board)), new_board_info
            elif enemy_is_evil and not i_am_evil:
                return -140, None, new_board_info
            elif not i_am_evil and not enemy_is_evil:
                if element_from_head_with_depth <= 1:  # Neck
                    if len(my_snake) - len(enemy_snake) > 2:  # I'll survive
                        # remove enemy and cut my snake
                        reward = len(enemy_snake) * SNAKE_REWARD
                        new_board = self.remove_enemy_and_cut_me(my_snake, i_am_evil, enemy_snake)
                        return reward, new_board, new_board_info
                    else:
                        return -140, None, new_board_info
                else:  # Body
                    return -140, None, new_board_info
            else:
                raise Exception("Can't be")
        elif next_el == element.APPLE:
            new_board = self.move_me(i_am_evil, with_apple=True)
            return 7, new_board, new_board_info
        elif next_el == element.FURY_PILL:
            new_board = self.move_me(i_am_evil)
            new_board_info = BoardInfo(new_board_info.tick, new_board_info.evil_countdown + FURY_COUNTDOWN)
            return 10, new_board, new_board_info
        elif next_el == element.GOLD:
            new_board = self.move_me(i_am_evil)
            return 20, new_board, new_board_info
        elif next_el == element.STONE:
            new_board = self.move_me(i_am_evil)
            if i_am_evil:
                return 4, new_board, new_board_info
            elif len(my_snake) < 5:  # eating stone: -3 length
                return -100, None, new_board_info
            else:
                return -10, new_board, new_board_info

        new_board = self.move_me(i_am_evil)
        print(f'Unexpected element: {next_el}')
        return 0, new_board, new_board_info

    def move_me(self, i_am_evil, with_apple=False):
        # print(f'Moving {self.action}')
        new_board = list(self.board._string)
        cur_head_pos = self.board.get_my_head
        next_head_pos = cur_head_pos + Direction.from_action(self.action).to_point()

        snake = self.board.get_my_snake[:]
        snake.insert(0, next_head_pos)

        if not with_apple:  # blank old tail pos
            tail_pos = snake.pop()
            i = self.board.get_shift_by_point(tail_pos)
            new_board[i] = element.NONE

        self.board.draw_snake(snake, new_board, i_am_evil)
        return Board(''.join(new_board))

    def remove_enemy_and_cut_me(self, my_snake, i_am_evil, enemy_snake):
        new_board = list(self.board._string)
        self.wipe_snake_from_board(new_board, enemy_snake)
        self.wipe_snake_from_board(new_board, my_snake)
        my_snake = my_snake[:len(my_snake) - len(enemy_snake)]
        self.move_snake(my_snake, Direction.from_action(self.action))
        self.board.draw_snake(my_snake, new_board, i_am_evil)
        return Board(''.join(new_board))

    def wipe_snake_from_board(self, board_list, snake):
        for point in snake:
            i = self.board.get_shift_by_point(point)
            board_list[i] = element.NONE
        return board_list

    def move_snake(self, snake, dir, with_apple=False):
        snake.insert(0, snake[0] + dir)
        if not with_apple:
            snake.pop()

    def get_next_tail_pos(self, cur_tail_pos: Point, cur_tail_el):
        if cur_tail_el == element.TAIL_END_DOWN:
            return cur_tail_pos + Direction.DOWN.to_point()
        elif cur_tail_el == element.TAIL_END_LEFT:
            return cur_tail_pos + Direction.LEFT.to_point()
        elif cur_tail_el == element.TAIL_END_UP:
            return cur_tail_pos + Direction.UP.to_point()
        elif cur_tail_el == element.TAIL_END_RIGHT:
            return cur_tail_pos + Direction.RIGHT.to_point()



