import datetime
import os

try:
    os.mkdir('logs')
except Exception:
    pass

def get_new_file():
    name = os.path.join('logs', datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S-%f') + '.log')
    file = open(name, 'w')
    return file


def finalize_file(file):
    file.close()
    size = os.path.getsize(file.name)
    if size < 3000:
        os.remove(file.name)


file = get_new_file()
while True:
    text = input()
    print(text)

    if '====restart====' in text:
        finalize_file(file)
        file = get_new_file()

    file.write(text)
    file.write('\n')
