import time
from collections import deque
from contextlib import contextmanager


@contextmanager
def timeit(name):
    start = time.time()
    yield
    print(f'{name} time: {time.time() - start:.4f}')


class SingleThreadQueue:
    def __init__(self):
        self.queue = deque()

    def put(self, obj):
        self.queue.append(obj)

    def get(self, block, timeout):
        return self.queue.popleft()

    def get_nowait(self):
        return self.queue.popleft()

    def empty(self):
        return not bool(self.queue)
