import multiprocessing as mp
import random
import time
from dataclasses import dataclass

from colorama import Fore, Style
from queue import Empty as QueueEmpty


@dataclass
class Workload:
    board: str
    reward: float
    actions: list

    # def __str__(self):
    #     return f'Workload'


def worker(i, queue, continue_processing_event, busy_event):
    while True:
        # print(f'w[{i}] checking event')
        continue_processing_event.wait()
        # print(f'w[{i}] clear busy event')
        busy_event.clear()
        try:
            # print(f'w[{i}] taking from queue')
            task = queue.get(True, 0.05)
            print(f'w[{i}] handling task {task}')
            time.sleep(random.randint(1,3))
            print(f'w[{i}] finished task')
            for i in random.sample(list(range(4)), random.randint(1, 3)):
                new_task = Workload(task.board, task.reward+1, task.actions + [i])
                queue.put(new_task)
        except QueueEmpty:
            pass
        finally:
            busy_event.set()


class Player:
    def __init__(self):
        PROCESS_COUNT = 2

        self.work_queue = mp.Queue()
        self.continue_processing_event = mp.Event()
        self.process_list = []
        self.process_busy_events = []
        for i in range(PROCESS_COUNT):
            busy_event = mp.Event()
            busy_event.set()
            process = mp.Process(target=worker, args=(i, self.work_queue, self.continue_processing_event, busy_event))
            self.process_list.append(process)
            self.process_busy_events.append(busy_event)

        print('Starting processes')
        for p in self.process_list:
            p.start()

    def drain_queue(self):
        result = []
        try:
            while True:
                result.append(self.work_queue.get_nowait())
        finally:
            return result

    def turn(self, board):
        print('turn start')

        self.work_queue.put(Workload(board, 0, []))
        self.continue_processing_event.set()
        print('Started processes')
        time.sleep(10) # doing job

        # Getting the results
        print('Sending signal to workers to stop')
        self.continue_processing_event.clear()  # stopping all processes

        print('Waiting for workers')
        for busy_event in self.process_busy_events:
            busy_event.wait()

        print('Draining queue')
        results = self.drain_queue()
        print(f"results:")
        for task in results:
            print(task)

        # time.sleep(5)
        # print(f'queue empty: {self.work_queue.empty()}')

        print('turn finish')


if __name__ == '__main__':
    player = Player()
    for i in range(10):
        start = time.time()
        print(f'{Fore.BLUE}Turn {i}{Style.RESET_ALL}')

        player.turn(f'aa{i}')

        delta = time.time() - start
        print(f'{Fore.BLUE}Turn {i} for {delta:.2f}{Style.RESET_ALL}')

    print('END')


