import multiprocessing as mp
import random
import time
import traceback
from dataclasses import dataclass
from queue import Empty as QueueEmpty

from engine.point import Direction, Point
from utils import SingleThreadQueue

import colorama

from engine.actions import Action
from engine.board import Board
from engine.world import World, BoardInfo

MINIMAX_ACTIONS = [Action.RIGHT, Action.DOWN, Action.LEFT, Action.UP]


@dataclass
class Workload:
    board: str
    board_info: BoardInfo
    reward: float
    actions: list

    def __str__(self):
        return (f'Workload('
                f'depth={len(self.actions)}, '
                f'board={"None" if self.board is None else "++++"}, '
                f'b_info={self.board_info}, '
                f'reward={self.reward:6.2f}, '
                f'actions={self.actions})')


def worker(i, queue, finished_queue, continue_processing_event, busy_event):
    while True:
        try:
            # if i < 3 and random.random() < 0.001:
            #     print(f'{colorama.Fore.RED}RASING EXCEPTION{colorama.Style.RESET_ALL}')
            #     raise Exception("MyException")

            # print(f'w[{i}] checking event')
            continue_processing_event.wait()
            # print(f'w[{i}] clear busy event')
            busy_event.clear()
            try:
                # print(f'w[{i}] taking from queue')
                task = queue.get(True, 0.05)
                worker_job(task, queue, finished_queue)
            except QueueEmpty:
                pass
            finally:
                busy_event.set()
        except Exception as ex:
            print(f"{colorama.Fore.RED}Exception in the worker: {ex}\n"
                  f"{traceback.format_exc()}"
                  f"{colorama.Style.RESET_ALL}")


def worker_job(task: Workload, queue, finished_queue):
    board = Board(task.board)
    # print(f'w[{i}] handling task {task}')
    depth = len(task.actions) + 1

    for cur_action in MINIMAX_ACTIONS:
        world = World(board, task.board_info, cur_action, depth)
        action_reward, next_board, next_board_info = world.calculate()
        action_reward *= 0.9 ** depth  # than later you do something than less reward you get
        next_board_str = next_board._string if next_board is not None else None
        new_workload = Workload(
            next_board_str, next_board_info, task.reward + action_reward, task.actions + [cur_action]
        )

        if new_workload.board is None:
            finished_queue.put(new_workload)
            # print(f'Worker[{i}]: {new_workload}')
        else:
            queue.put(new_workload)


def drain_queue(queue):
    result = []
    try:
        while True:
            result.append(queue.get(True, 0.05))
    except QueueEmpty:
        return result
    finally:
        return result


class Player:
    HANDLING_TIME = 0.5
    HANDLING_TIMEOUT = 0.8

    def __init__(self, process_count=4):
        """
        :param process_count: if parameter 0 than it will work synchronously in Main process
        """
        self.process_count = process_count
        self.tick = 0
        self.timeout_amount = 0
        self.board_info = BoardInfo(0, 0)
        self.board = Board('    ')
        self.init_processes()

    def init_processes(self):
        if self.is_single_threaded():
            self.work_queue = SingleThreadQueue()
            self.finished_queue = SingleThreadQueue()
        else:
            self.work_queue = mp.Queue()
            self.finished_queue = mp.Queue()

        self.continue_processing_event = mp.Event()
        self.process_list = []
        self.process_busy_events = []
        for i in range(self.process_count):
            busy_event = mp.Event()
            busy_event.set()
            process = mp.Process(target=worker, args=(i, self.work_queue, self.finished_queue, self.continue_processing_event, busy_event))
            self.process_list.append(process)
            self.process_busy_events.append(busy_event)

        print('Starting processes')
        for p in self.process_list:
            p.start()

    def is_single_threaded(self):
        return self.process_count == 0

    def turn(self, board_string: str, calc_amount=100):
        """
        :param board_string:
        :param calc_amount:  if Player create with process_count=0, than it will calculate specified amount times
        :return:
        """
        if '~&' in board_string:
            self.restart()

        if self.tick:
            print(f'Timeout ratio: {self.timeout_amount / self.tick * 100:.2f}%')
        self.handle_start_time = time.time()

        self.tick += 1
        self.board = Board(board_string)
        self.board.print_board()

        self._check_queues_leftovers()

        self.work_queue.put(Workload(self.board._string, self.board_info, 0, []))
        # Starting workers
        print(f'Starting workers. Tick: {self.tick}')
        self.continue_processing_event.set()

        # Workers doing their job
        if self.is_single_threaded():
            self.signle_thread_calculation(calc_amount)
        else:
            print('Sleeping')
            time.sleep(self.HANDLING_TIME)

        # Stopping workers
        start_time_stopping_workers = time.time()
        print('Sending signal to workers to stop')
        self.continue_processing_event.clear()  # stopping all processes
        print('Waiting for workers')
        for busy_event in self.process_busy_events:
            busy_event.wait(0.1)
        print(f'Stopping workers took {time.time() - start_time_stopping_workers:.3f}s')

        # Getting the results
        possible_scenarios = drain_queue(self.work_queue) + drain_queue(self.finished_queue)
        print(f'Result size: {len(possible_scenarios)}')
        self.result_size = len(possible_scenarios)

        max_reward = max(task.reward for task in possible_scenarios)
        best_task_list = [task for task in possible_scenarios if task.reward == max_reward]
        print(f'Best_task_list len: {len(best_task_list)}')
        if len(best_task_list) > 50:
            best_task = self.choose_closer_to_apples(best_task_list)
        else:
            best_task = random.choice(best_task_list)
        actions = best_task.actions
        print(f'Chosen task: {best_task}')
        reward, board, self.board_info = World(self.board, self.board_info, actions[0]).calculate()
        print(f'New board info: {self.board_info}')

        handling_time = time.time() - self.handle_start_time
        print(f'Handling time: {handling_time}')

        if handling_time > self.HANDLING_TIMEOUT:
            self.timeout_amount += 1
            print(f"{colorama.Fore.RED}Timeout detected, stop calculating in depth!{colorama.Style.RESET_ALL}")

        return actions[0].value

    def restart(self):
        self.tick = 0
        self.timeout_amount = 0
        self.board_info = BoardInfo(0, 0)
        print(f'{colorama.Fore.BLUE}====restart===={colorama.Style.RESET_ALL}')

    def _check_queues_leftovers(self):
        if not self.work_queue.empty() or not self.finished_queue.empty():
            work_queue_leftover = drain_queue(self.work_queue)
            finished_queue_leftover = drain_queue(self.finished_queue)
            if work_queue_leftover:
                print(f'{colorama.Fore.RED}work_queue_leftover: {work_queue_leftover}{colorama.Style.RESET_ALL}')
            if finished_queue_leftover:
                print(f'{colorama.Fore.RED}finished_queue_leftover: {finished_queue_leftover}{colorama.Style.RESET_ALL}')

    def signle_thread_calculation(self, calc_amount):
        for _ in range(calc_amount):
            if self.work_queue.empty():
                break
            task = self.work_queue.get_nowait()
            worker_job(task, self.work_queue, self.finished_queue)

    def terminate(self):
        for process in self.process_list:
            process.terminate()

    def choose_closer_to_apples(self, best_task_list):
        try:
            apples = self.board.get_apples()
            apples_x = int(sum(p.x for p in apples) / len(apples))
            apples_y = int(sum(p.y for p in apples) / len(apples))
            apple_center = Point(apples_x, apples_y)
            print(f'Apple center: {apple_center}')

            task_lengths = []
            for task in best_task_list:
                final_point = self.board.get_my_snake[0]
                for action in task.actions:
                    final_point += Direction.from_action(action).to_point()
                task_lengths.append((final_point - apple_center).length())

            max_length = max(task_lengths)
            weights = [max_length - length for length in task_lengths]

            best_task, best_weight = max(zip(best_task_list, weights), key=lambda tpl: tpl[1])

            return best_task
        except Exception as ex:
            print(f"{colorama.Fore.RED}Exception in choose_closer_to_apples: {ex}\n"
                  f"{traceback.format_exc()}"
                  f"{colorama.Style.RESET_ALL}")
            return random.choice(best_task_list)
